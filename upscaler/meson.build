pkgdatadir = join_paths(get_option('prefix'), get_option('datadir'), meson.project_name())
moduledir = join_paths(pkgdatadir, 'upscaler')
gnome = import('gnome')

blueprints = custom_target('blueprints',
  input: files(
    'gtk/help-overlay.blp',
    'gtk/window.blp',
    'gtk/queue-row.blp',
    'gtk/dimension-label.blp',
    'gtk/scale-spin-button.blp',
  ),
  output: '.',
  command: [find_program('blueprint-compiler'), 'batch-compile', '@OUTPUT@', '@CURRENT_SOURCE_DIR@', '@INPUT@'],
)

python = import('python')

conf = configuration_data()
conf.set('PYTHON', python.find_installation('python3').full_path())
conf.set('VERSION', meson.project_version())
conf.set('localedir', join_paths(get_option('prefix'), get_option('localedir')))
conf.set('pkgdatadir', pkgdatadir)
conf.set('app_name', app_name)
conf.set('app_version', app_version)
conf.set('profile', profile)
conf.set('app_id', app_id)
conf.set('unsuffixed_app_id', unsuffixed_app_id)
conf.set('app_icon', app_icon)
conf.set('profile', profile)


configure_file(
  input: 'upscaler.in',
  output: 'upscaler',
  configuration: conf,
  install: true,
  install_dir: get_option('bindir')
)

configure_file(
  input: 'app_profile.py.in',
  output: 'app_profile.py',
  configuration: conf,
  install: true,
  install_dir: moduledir
)

gresource_file = configure_file(
  input: 'upscaler.gresource.xml.in',
  output: 'upscaler.gresource.xml',
  configuration: conf
)

upscaler_sources = [
  '__init__.py',
  'main.py',
  'window.py',
  'utils.py',
  'threading.py',
  'controller.py',
  'constants.py',
  'exceptions.py',
  'queue_row.py',
  'dimension_label.py',
  'scale_spin_button.py',
]

gnome.compile_resources('upscaler',
  gresource_file,
  gresource_bundle: true,
  install: true,
  install_dir: pkgdatadir,
  dependencies: [blueprints, appstream_file],
)

install_data(upscaler_sources, install_dir: moduledir)
